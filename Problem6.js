
//    Create a function to retrieve and display the first hobby of each individual in the dataset.



function firstHobby(array) {
    if (Array.isArray(array)) {
        const hobbies = [];
        for (let index = 0; index < array.length; index++) {
            if (array[index].hobbies.length >= 1) {
                hobbies.push(array[index].hobbies[0]);
            }
        }
        return hobbies;
    }
    else {
        return null;
    }
}

module.exports = firstHobby;

