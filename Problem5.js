
//    Implement a loop to access and print the ages of all individuals in the dataset.

function ageOfAllIndividual(array) {
    if (Array.isArray(array)) {
        const ages = [];
        for (let index = 0; index < array.length; index++) {
            ages.push(array[index].age);
        }
        return ages;
    }
    else {
        return null;
    }
}

module.exports = ageOfAllIndividual;


