

function getHobbiesByAge(array, age) {
    if (Array.isArray(array)) {
        let hobbies = [];
        for (index = 0; index < array.length; index++) {
            if (array[index].age == age) {
                hobbies.push(array[index].hobbies);
            }
        }
        return hobbies;
    }
    else{
        return [];
    }

}

module.exports = getHobbiesByAge;