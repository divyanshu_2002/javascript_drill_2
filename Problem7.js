
//    Write a function that accesses and prints the names and email addresses of individuals aged 25.

function DetailsOfAge(array) {
    if (Array.isArray(array)) {
        const nameAndEmail = [];
        for (let index = 0; index < array.length; index++) {
            if (array[index].age === 25) {
                let temp = {};
                temp.name = array[index].name;
                temp.email = array[index].email;
                nameAndEmail.push(temp);

            }
        }
        return nameAndEmail;
    }
    else {
        return null;
    }
}

module.exports = DetailsOfAge;
