

function getCityAndCountry(array) {
    if (Array.isArray(array)) {
        let data = [];
        for (let index = 0; index < array.length; index++) {
            let tempData = {};
            tempData.city = array[index].city;
            tempData.country = array[index].country;
            data.push(tempData);
        }
        return data;
    }
    else{
        return null;
    }
}


module.exports = getCityAndCountry;
