//    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.

function studentInAustralia(array) {
    const students = [];
    if (Array.isArray(array)) {
        for (let index = 0; index < array.length; index++) {
            if (array[index].isStudent == true && array[index].country == "Australia") {
                students.push(array[index].name);
            }
        }
        return students;
    }
    else{
        return students;
    }

}

module.exports = studentInAustralia;