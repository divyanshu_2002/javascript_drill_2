const studentInAustralia = require("../Problem3");
const array = require("../data.json");

try {
    const students = studentInAustralia(array);
    if (students) {
        console.log(`The students living in australia are ${students}`);
    }
    else {
        console.log(`There are no students in australia`);
    }

}
catch (error) {
    console.log(error.message);
}