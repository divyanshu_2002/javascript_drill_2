const getHobbies = require("../Problem2");
const array = require("../data.json");

try {
    const hobbies = getHobbies(array, 30);
    if (hobbies) {
        console.log(`The hobbies of the person with age 30 are ${hobbies}`);
    }
    else {
        console.log("There are no person with the age 30");
    }
}
catch (error) {
    console.log(error.message);
}