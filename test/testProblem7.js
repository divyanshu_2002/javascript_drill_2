const DetailsOfAge = require("../Problem7");
const array = require("../data.json");

try {
    const nameAndEmailDetails = DetailsOfAge(array);
    if (nameAndEmailDetails) {
        console.log(`The name and email of the person with age 25 are`);
        console.log(nameAndEmailDetails);
    }
    else {
        console.log(`pass the array as arguments to get the details`);
    }

}
catch (error)0{
    console.log(error.message);
}