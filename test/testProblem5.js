const ageOfAllIndividual = require("../Problem5");
const array = require("../data.json");

try {
    const ages = ageOfAllIndividual(array);
    if (ages) {
        console.log(`Age of each individuals are ${ages}`);
    }
    else {
        console.log(`Pass the valid array and with the property age`);
    }

}
catch (error) {
    console.log(error.message);
}
