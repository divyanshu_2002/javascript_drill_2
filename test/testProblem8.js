const getCityAndCountry = require("../Problem8");
const array = require("../data.json");

try {

    const result = getCityAndCountry(array);
    if (result) {
        console.log(`The city and country of each individuals are`);
        console.log(result);
    }
    else {
        console.log(`Pass the array as arguments to get the city and country`);
    }

}
catch (error) {
    console.log(error.message);
}