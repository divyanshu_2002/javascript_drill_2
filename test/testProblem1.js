const getEmail = require("../Problem1");
const array = require("../data.json");

try {
    const emails = getEmail(array);
    if (emails) {
        console.log(`The emails of all person are ${emails}`)
    }
}
catch (error) {
    console.log(error.message);
}