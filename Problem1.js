
function getEmail(array) {
   const emails = [];

   if (Array.isArray(array)) {
      for (let index = 0; index < array.length; index++) {
         emails.push(array[index].email);
      }
      return emails;
   }
   else {
      return [];
   }

}

module.exports = getEmail

